#!/bin/bash

set -exu

ARCH=$(arch)
# Convert the architectures to the ones used by the containers
if [[ "${ARCH}" == "aarch64" ]]; then
  ARCH="arm64"
elif [[ "${ARCH}" == "x86_64" ]]; then
  ARCH="amd64"
else
  echo "ERROR: arch ${ARCH} not supported" >&2
  exit 1
fi

IMAGE_NAME="${IMAGE_NAME:-qa}"
CONTAINERFILE="${IMAGE_NAME}/Containerfile"
IMAGE_VERSION="${IMAGE_VERSION:-0.1}"
DISTRO_NAME="${COMPOSE_DISTRO:-AutoSD}"
CONTAINER_IMAGE="${OCI_SOURCE:-quay.io/automotive-toolchain/autosd}"
CONTAINER_TAG="${OCI_SORCE_TAG:-qemu-minimal}"
PUBLISH_TAG="${CONTAINER_IMAGE}:qemu-${IMAGE_NAME}-${ARCH}"

if [[ ! -f "${CONTAINERFILE}" ]]; then
  echo "ERROR: the file ${CONTAINERFILE} doesn't exist" >&2
  exit 1
fi

# If the SSH_KEY exists, then add it to the root keys file
if [[ -v SSH_KEY ]]; then
  echo "${SSH_KEY}" > "${IMAGE_NAME}/root.keys"
fi

# Login
buildah login --username "${CONTAINER_REGISTRY_USER}" --password "${CONTAINER_REGISTRY_PASSWORD}" "${CONTAINER_REGISTRY}"

# Build the image
buildah bud \
        --build-arg NAME="${DISTRO_NAME} - ${IMAGE_NAME}" \
        --build-arg VERSION="${IMAGE_VERSION}" \
        --build-arg CONTAINER_IMAGE="${CONTAINER_IMAGE}" \
        --build-arg CONTAINER_TAG="${CONTAINER_TAG}" \
	--label "quay.expires-after=${QUAY_EXPIRES_AFTER:-never}" \
        -f "${IMAGE_NAME}/Containerfile" \
        -t  "${PUBLISH_TAG}" \
        "${IMAGE_NAME}"

# Show the manifest
buildah inspect "${PUBLISH_TAG}"

# Push it to the registry
buildah push "${PUBLISH_TAG}"
